<?php

namespace App\Http\Controllers;

use App\Video;
use FFMpeg\Media\Gif;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\Coordinate\Dimension;
use FFMpeg\Filters\Video\ClipFilter;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Requests\StoreVideoRequest;
use Pbmedia\LaravelFFMpeg\FFMpegFacade as FFMpeg;

class VideoController extends Controller
{
    public function store(StoreVideoRequest $request){

    	//store to s3 and get url
    	$video = Video::create([
    		'title' => $request->input('title'),
    		'video_url' => '',
            'gif_url' => '',
    		'user_id' => Auth::id()
    	]);

        //store video locally in order to be able to generate gif with ffmpeg

    	$folder_path = 'videos/'.$video->id;
        $video_name = Str::slug($request->input('title'));  

        $video_local_url = Storage::disk('local')->putFileAs(
            $folder_path, 
            $request->file('video'),
            $video_name.'.webm'
        );

        //generate gif and store it locally

        $full_video_path = $folder_path.'/'.$video_name;
        $gif_path = storage_path('app/'.$full_video_path.'.gif');

        FFMpeg::fromDisk('local')
            ->open($full_video_path.'.webm')
            ->gif(TimeCode::fromSeconds(0), new Dimension(640, 480), 1)
            ->save($gif_path);

        //get gif contents

        $gif = Storage::disk('local')->get($full_video_path.'.gif');

        //storeg both files on s3

        $video_url = Storage::disk('s3')->putFileAs(
            $folder_path, 
            $request->file('video'),
            $video_name.'.webm'
        );

        $gif_url = Storage::disk('s3')->put(
            $folder_path.'/'.$video_name.'.gif', 
            $gif
        );

    	$video->video_url = $video_url;
        $video->gif_url = $folder_path.'/'.$video_name.'.gif';
    	$video->save();

        //delete local files

        Storage::disk('local')->delete($video_url);
        Storage::disk('local')->delete($folder_path.'/'.$video_name.'.gif');

    	return response()->json([
    		'status' => 'success',
    		'video_url' => $video_url,
            'gif_url' => $folder_path.'/'.$video_name.'.gif'
    	]);

    }
}
