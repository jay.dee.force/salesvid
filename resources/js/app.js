
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import "jquery";

require('./bootstrap');


 // Javascript to enable link to tab
var hash = document.location.hash;

if (hash) {
    $('.nav-pills a[href="' + hash + '"]').tab('show');
} 

// Change hash for page-reload
$('.nav-pills a').on('shown.bs.tab', function (e) {
    window.location.hash = e.target.hash;
    window.scrollTo(0, 0);
})
