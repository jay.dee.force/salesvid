@extends('layouts.app')

@section('content')
<div id="app" class="container">
    <div class="row justify-content-center">
        <div class="col-md-3 mb-3">
            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                <a class="nav-link active" id="v-pills-record-tab" data-toggle="pill" href="#v-pills-record" role="tab" aria-controls="v-pills-record">Record video</a>
                <a class="nav-link" id="v-pills-uploads-tab" data-toggle="pill" href="#v-pills-uploads" role="tab" aria-controls="v-pills-uploads">Uploads</a>
            </div>
        </div>
        <div class="col-md-9">
            <div class="tab-content" id="v-pills-tabContent">
                <div class="tab-pane fade show active" id="v-pills-record" role="tabpanel" aria-labelledby="v-pills-record-tab">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">Record video</div>

                            <div class="card-body">
                                <input v-model="title" class="form-control mb-3" type="text" placeholder="Enter a name for the video">
                                <video style="width: 100%" id="myVideo" class="video-js vjs-default-skin"></video>
                                <button @click="uploadVideo" id="uploadVideo" class="btn btn-primary mt-4 d-none">Upload Video</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="v-pills-uploads" role="tabpanel" aria-labelledby="v-pills-uploads-tab">
                    <div class="tab-pane fade show active" id="v-pills-record" role="tabpanel" aria-labelledby="v-pills-record-tab">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">Uploads</div>
                                <div class="card-body">
                                    @foreach(Auth::user()->videos()->latest()->get() as $video)

                                    <div class="card mb-2">
                                      <div class="card-body">
                                        <h5 class="card-title">{{$video->title}}</h5>
                                         <a target="_blank" href="{{ $video->full_video_url }}" class="card-link">{{ $video->full_video_url }}</a>
                                         <a target="_blank" href="{{ $video->full_gif_url }}" class="card-link">{{ $video->full_gif_url }}</a>
                                      </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">...</div>
                <div class="tab-pane fade" id="v-pills-settings" role="tabpanel" aria-labelledby="v-pills-settings-tab">...</div>
            </div>
        </div>
    </div>
</div>

@push('scripts')

<script>
    var app = new Vue({
      el: '#app',
      mounted: function(){

        this.player = videojs('myVideo', {
            // video.js options
            controls: true,
            loop: false,
            fluid: false,
            width: 320,
            height: 400,
            plugins: {
                record: {
                    image: false,
                    audio: true,
                    video: true,
                    debug: true,
                    maxLength: 900,
                }
            }
        });

        this.player.on('startRecord', function() {
            $('#uploadVideo').addClass('d-none');
        });

        this.player.on('finishRecord', function() {
            $('#uploadVideo').removeClass('d-none');
        });
      },
      data: {
        player: null,
        title: null
      },
      methods: {
        uploadVideo: function(){

            if(this.title == '' || this.title == null){
                
                alert("Please specify a name for the video");

            }else{

                var formData = new FormData();
                var blob = this.player.recordedData;

                formData.append('video', blob, blob.name);
                formData.append('title', this.title);

                axios.post('video', formData).then(function (response) {

                    alert("Video uploaded successfully");
                
                })
                .catch(function (error) {
                
                    alert("There was an error");
                    window.location.reload();
                
                });

            }
        }
      }
    })

</script>

@endpush

@endsection
